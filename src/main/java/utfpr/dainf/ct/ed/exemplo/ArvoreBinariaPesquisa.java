package utfpr.dainf.ct.ed.exemplo;
import java.util.ArrayDeque;
import java.util.Stack;


/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Exemplo de implementação de árvore binária de pesquisa.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <E> O tipo do valor armazenado nos nós da árvore
 */
public class ArvoreBinariaPesquisa<E extends Comparable<E>> extends ArvoreBinaria<E> {
    public ArvoreBinariaPesquisa<E> pai;
    protected Stack<ArvoreBinariaPesquisa<E>> pilha;
    protected ArvoreBinariaPesquisa<E> esquerda;
    protected ArvoreBinariaPesquisa<E> direita;
    protected ArvoreBinariaPesquisa<E> ultimoVisitado;
    
    
    // para percurso iterativo
    private boolean inicio = true;
    private ArvoreBinariaPesquisa<E> noPos;

    
    private void inicializaPilha() {
        if (pilha == null) {
            pilha = new Stack<>();
        }
    }
    
    public void reinicia() {
        inicializaPilha();
        pilha.clear();
        ultimoVisitado = this;
        inicio = true;
    }
    
    /**
     * Retorna a árvore esqueda.
     * @return A árvore esquerda.
     */
    protected ArvoreBinaria<E> getEsquerda() {
        return esquerda;
    }

    /**
     * Retorna a árvore direita.
     * @return A árvore direita.
     */
    protected ArvoreBinaria<E> getDireita() {
        return direita;
    }

    /**
     * Inicializa a árvore esquerda.
     * @param esquerda A árvore esquerda.
     */
    protected void setEsquerda(ArvoreBinariaPesquisa<E> esquerda) {
        this.esquerda = esquerda;
    }

    /**
     * Inicializa a árvore direita.
     * @param direita A árvore direita.
     */
    protected void setDireita(ArvoreBinariaPesquisa<E> direita) {
        this.direita = direita;
    }
     
    
    /**
     * Insere uma subárvore à esquerda deste nó.
     * A subárvore à esquerda deste nó é inserida na folha mais à esquerda
     * da subárvore inserida.
     * @param a A subárvore a ser inserida.
     * @return A subárvore inserida.
     */
    public ArvoreBinariaPesquisa<E> insereEsquerda(ArvoreBinariaPesquisa<E> a) {
        ArvoreBinariaPesquisa<E> e = esquerda;
        ArvoreBinariaPesquisa<E> x = a;
        esquerda = a;
        while (x.esquerda != null)
            x = x.esquerda;
        x.esquerda = e;
        return a;
    }
    
    /**
     * Insere uma subárvore à direita deste nó.
     * A subárvore à direita deste nó é inserida na folha mais à direita
     * da subárvore inserida.
     * @param a A subárvore a ser inserida.
     * @return A subárvore inserida.
     */
    public ArvoreBinariaPesquisa<E> insereDireita(ArvoreBinariaPesquisa<E> a) {
        ArvoreBinariaPesquisa<E> d = direita;
        ArvoreBinariaPesquisa<E> x = a;
        direita = a;
        while (x.direita != null)
            x = x.direita;
        x.direita = d;
        return a;
    }
    
    
    /**
     * Visita os nós da subárvore em-ordem.
     * @param raiz A raiz da subárvore
     */
    public void visitaEmOrdem(ArvoreBinariaPesquisa<E> raiz) {
        if (raiz != null) {
            visitaEmOrdem(raiz.esquerda);
            visita(raiz);
            visitaEmOrdem(raiz.direita);
        }
    }
    
    /**
     * Retorna o valor do próximo nó em-ordem.
     * @return O valor do próximo nó em-ordem.
     */
    public ArvoreBinariaPesquisa<E> proximoEmOrdem() {
        ArvoreBinariaPesquisa<E> resultado = null;
        if (inicio) {
            reinicia();
            inicio = false;
        }
        if (!pilha.isEmpty() || ultimoVisitado != null) {
            while (ultimoVisitado != null) {
                pilha.push(ultimoVisitado);
                ultimoVisitado = ultimoVisitado.esquerda;
            }
            ultimoVisitado = pilha.pop();
            resultado = ultimoVisitado;
            ultimoVisitado = ultimoVisitado.direita;
        }
        return resultado;
    }
    
    /**
     * Visita os nós da árvore em-ordem a partir da raiz.
     */
    public void visitaEmOrdem() {
        visitaEmOrdem(this);
    }
    
    /**
     * Cria uma árvore com valor nulo na raiz.
     */
    public ArvoreBinariaPesquisa() {
    }

    /**
     * Cria uma árvore com o valor especificado na raiz.
     * @param valor O valor armazenado na raiz.
     */
    public ArvoreBinariaPesquisa(E valor) {
        super(valor);
    }

    /**
     * Inicializa o nó pai deste nó.
     * @param pai O nó pai.
     */
    protected void setPai(ArvoreBinariaPesquisa<E> pai) {
        this.pai = pai;
    }

    /**
     * Retorna o nó pai deste nó.
     * @return O nó pai.
     */
    protected ArvoreBinariaPesquisa<E> getPai() {
        return pai;
    }

    /**
     * OK
     * Retorna o nó da árvore cujo valor corresponde ao especificado.
     * @param valor O valor a ser localizado.
     * @return A raiz da subárvore contendo o valor ou {@code null}.
     */
    public ArvoreBinariaPesquisa<E> pesquisa(E valor)
    {
        ArvoreBinariaPesquisa<E> resultado = this;
        while( resultado != null && valor.compareTo(resultado.valor) != 0  )
        {
            if( valor.compareTo(resultado.valor) < 0 && resultado.esquerda != null  )
                resultado = (ArvoreBinariaPesquisa)resultado.esquerda;
            else if(  valor.compareTo(resultado.valor) > 0 && resultado.direita != null )
                resultado = (ArvoreBinariaPesquisa)resultado.direita;
            else
                resultado = null;
        }
        return resultado;    
    }
    
    
    /**
     * OK
     * Retorna o nó da árvore com o menor valor.
     * @return A raiz da subárvore contendo o valor mínimo
     */
    public ArvoreBinariaPesquisa<E> getMinimo()
    {
        ArvoreBinariaPesquisa<E> resultado = this;
        while( resultado.esquerda != null ) {
            resultado = (ArvoreBinariaPesquisa)resultado.esquerda; }
        return resultado;
    }

    /**
     * OK
     * Retorna o nó da árvore com o maior valor.
     * @return A raiz da subárvore contendo o valor máximo
     */
    public ArvoreBinariaPesquisa<E> getMaximo()
    {
        ArvoreBinariaPesquisa<E> resultado = this;
        while( resultado.direita != null ) {
            resultado = (ArvoreBinariaPesquisa)resultado.direita; }
        return resultado;
    }

    /**
     * OK
     * Retorna o nó sucessor do nó especificado.
     * @param no O nó cujo sucessor desejamos localizar
     * @return O sucessor do no ou {@null}.
     */
    public ArvoreBinariaPesquisa<E> sucessor(ArvoreBinariaPesquisa<E> no)
    {
        ArvoreBinariaPesquisa<E> resultado = no;
        if( resultado != null && resultado.direita != null )
        {
            resultado = (ArvoreBinariaPesquisa)resultado.direita;
            if( resultado.esquerda != null )
            {
                while( resultado.esquerda != null ) {
                    resultado = (ArvoreBinariaPesquisa)resultado.esquerda; }
            }
        }
        else if( resultado != null && resultado.direita == null )
        {
            while( resultado.pai != null )
            {
                resultado = resultado.pai;
                if( resultado.esquerda != null && (resultado.esquerda.valor).compareTo(no.valor) == 0 ) {
                    break; }
                else if( (resultado.valor).compareTo(this.valor) == 0 && (no.valor).compareTo(this.valor) > 0 )
                {
                    resultado = null;
                    break;
                }
            }
        }
        return resultado;
    }

    /**
     * OK
     * Retorna o nó predecessor do nó especificado.
     * @param no O nó cujo predecessor desejamos localizar
     * @return O predecessor do nó ou {@null}.
     */
    public ArvoreBinariaPesquisa<E> predecessor(ArvoreBinariaPesquisa<E> no)
    {
        ArvoreBinariaPesquisa<E> resultado = no;
        if( resultado != null && resultado.esquerda != null ) 
        {
            resultado = (ArvoreBinariaPesquisa)resultado.esquerda;
            if( resultado.direita != null )
            {
                while( resultado.direita != null ) {
                    resultado = (ArvoreBinariaPesquisa)resultado.direita; }
            }
        }
        else if( resultado != null && resultado.esquerda == null )
        {
            while( resultado.pai != null )
            {
                resultado = resultado.pai;
                if( (resultado.valor).compareTo(no.valor) < 0 ) {
                    break; }
                else if( (resultado.valor).compareTo(this.valor) == 0 && (no.valor).compareTo(this.valor) < 0 )
                {
                    resultado = null;
                    break;
                }
            }
        }
        return resultado;
    }

    /**
     * Insere um nó contendo o valor especificado na árvore.
     * @param valor O valor armazenado no nó.
     * @return O nó inserido
     */
    public ArvoreBinariaPesquisa<E> insere(E valor)
    {
        ArvoreBinariaPesquisa<E> resultado = this;
        ArvoreBinariaPesquisa<E> servo = null;
        
        while(valor.compareTo(resultado.valor) != 0)
        {
            if( valor.compareTo(resultado.valor) < 0 && resultado.esquerda != null  ) 
                resultado = (ArvoreBinariaPesquisa)resultado.esquerda;
            else if( valor.compareTo(resultado.valor) < 0 && resultado.esquerda == null )
            {
                servo = (ArvoreBinariaPesquisa)resultado;
                resultado.insereEsquerda(new ArvoreBinariaPesquisa<>(valor));
                resultado = (ArvoreBinariaPesquisa)resultado.esquerda;
                resultado.pai = (ArvoreBinariaPesquisa)servo;
            }
            else if( valor.compareTo(resultado.valor) > 0 && resultado.direita != null )
               resultado = (ArvoreBinariaPesquisa)resultado.direita;
            else if( valor.compareTo(resultado.valor) > 0 && resultado.direita == null )
            {
                servo = (ArvoreBinariaPesquisa)resultado;
                resultado.insereDireita(new ArvoreBinariaPesquisa<>(valor));
                resultado = (ArvoreBinariaPesquisa)resultado.direita;
                resultado.pai = (ArvoreBinariaPesquisa)servo;
            }
        }
        return resultado;
    }

    /*
     * Exclui o nó especificado da árvore.
     * Se a raiz for excluída, retorna a nova raiz.
     * @param no O nó a ser excluído.
     * @return A raiz da árvore
     */
    public ArvoreBinariaPesquisa<E> exclui(ArvoreBinariaPesquisa<E> no)
    {
       
        no = pesquisa(no.valor);
        ArvoreBinariaPesquisa<E> resultado = no;
       
       
       if( resultado.direita == null && resultado.esquerda == null )
       {
           if( resultado.pai == null )
           {
               no.valor = null;
           }
           else
           {
               if( resultado == resultado.pai.direita )
                    resultado.pai.direita = null;
                else if( resultado == resultado.pai.esquerda )
                    resultado.pai.esquerda = null;
           }
       }
       
        
        else if( resultado.direita != null && resultado.esquerda == null )
        {
            if( resultado.pai != null ) 
            {
                resultado.direita.pai = resultado.pai;
                if( resultado == resultado.pai.direita )
                    resultado.pai.direita = resultado.direita;
                else if( resultado == resultado.pai.esquerda )
                    resultado.pai.esquerda = resultado.direita;
            }
            else
            {
                resultado.valor = resultado.direita.valor;
                resultado.direita = null;
            }
        }
       else if( resultado.direita == null && resultado.esquerda != null )
        {
            if( resultado.pai != null )
            {
                resultado.esquerda.pai = resultado.pai;
                if( resultado == resultado.pai.direita )
                    resultado.pai.direita = resultado.esquerda;
                else if( resultado == resultado.pai.esquerda )
                    resultado.pai.esquerda = resultado.esquerda;
            }
            else
            {
                resultado.valor = resultado.esquerda.valor;
                resultado.esquerda = null;
            }
        }
       
       
       else if( resultado.direita != null && resultado.esquerda != null )
       {
           while( resultado.direita != null && resultado.esquerda != null )
           {
               resultado = sucessor(resultado);
           }
           exclui(resultado);
           no.valor = resultado.valor;

       }
       return this; 
    }
}
